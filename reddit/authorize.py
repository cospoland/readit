import praw
from readit_version import VERSION_STRING, APP_NAME, MAINTAINER

class REDDIT_AUTHORIZE:

  CLIENT_ID = None
  CLIENT_SECRET = None
  INSTANCES = {}

  def authorize_individual(self, user):
    print('[reddit-authorize]', user['name'])
    self.INSTANCES[user['name']] = praw.Reddit(client_id=self.CLIENT_ID,
                     client_secret=self.CLIENT_SECRET,
                     refresh_token=user['refresh_token'],
                     user_agent=' '.join([':'.join([APP_NAME, VERSION_STRING]), ''.join(['(by /u/', MAINTAINER, ')'])]))

  def authorize_everyone(self, users):
    for user in users: self.authorize_individual(user)
