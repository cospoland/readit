import psycopg2
import os
import reddit
from yaml import load as yamlload
try:
  from yaml import CLoader as YAMLLoader
except ImportError:
  from yaml import Loader as YAMLLoader

DATABASE = None
DBCURSOR = None

if __name__ == "__main__":
  stream = open('config.yml', 'r')
  data = yamlload(stream, Loader=YAMLLoader)
  stream.close()

  print('Connecting to the database')
  DATABASE = psycopg2.connect("dbname={} user={} password={} sslmode={} host={}".format(data['database']['name'],
                               data['database']['user'],
                               data['database']['password'],
                               data['database']['ssl'],
                               data['database']['host']
                             ))
  DBCURSOR = DATABASE.cursor()

  if data['database']['migrate']:
    print('Performing database migration')
    for file in os.listdir('db/migrate'):
      stream = open('/'.join(['db/migrate', file]), 'r')
      migration = yamlload(stream, Loader=YAMLLoader)
      run = migration['always']
      if not run:
        DBCURSOR.execute("""SELECT ref FROM Migrations WHERE ref = %s""", (migration['ref'],))
        run = DBCURSOR.fetchone()[0] != migration['ref']
      if run:
        print('->', migration['comment'], '[', file, ']')
        DBCURSOR.execute(migration['statement'])
        DATABASE.commit()
        DBCURSOR.execute("""INSERT INTO Migrations VALUES(%s)""", (migration['ref'],))
        DATABASE.commit()
        print('--> Commiting')
      del migration

  print('Connecting to Redis')

  print('Scheduling Redis clean-up task')

  print('Authorizing all existing users')
  DBCURSOR.execute("SELECT name, refresh_token FROM Users;")
  instances = reddit.REDDIT_AUTHORIZE()
  instances.CLIENT_ID = data['reddit']['client_id']
  instances.CLIENT_SECRET = data['reddit']['client_secret']
  instances.authorize_everyone(DBCURSOR.fetchall())

  print('Spinning up callback server')

  print('Initializing providers')

  print('Scheduling tasks')
